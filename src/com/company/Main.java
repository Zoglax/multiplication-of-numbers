package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a, b, k=1;

        System.out.println("Input 1-st number");
        a=scanner.nextInt();
        System.out.println("Input 2-nd number");
        b=scanner.nextInt();

        while (a<=b)
        {
            k=k*a;
            a++;
        }
        System.out.println("Multiplication of numbers: "+k);
    }
}
